webpackJsonp([2],{

/***/ 102:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InfoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the InfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var InfoPage = /** @class */ (function () {
    function InfoPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    InfoPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad InfoPage');
    };
    InfoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-info',template:/*ion-inline-start:"C:\Users\hicham\Documents\projects\school\smartChair\smartChairApp\src\pages\info\info.html"*/'<ion-header>\n  <ion-toolbar>\n    <ion-title>\n      info\n    </ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content padding>\n<ion-list>\n  <ion-title class="wrapper-logo">\n    <ion-img class="img-logo" src="assets/imgs/logo.jpg"></ion-img>\n  </ion-title>\n  <ion-item>\n    <div class="content-title">\n      Info\n    </div>\n    <div class="content-body" padding margin-horizontal text-center >\n      <h2 class="content-body-title">About us</h2>\n      <p text-wrap>\n        We from “Take A Seat” strive to give you the ultimate experience that a chair is possible to deliver.\n        We are a young innovative company who are always open for new idea’s, so do not hesitate to contact us.\n      </p>\n    </div>\n    <div class="content-body" padding margin-horizontal text-center >\n        <h2 class="content-body-title">Contact</h2>\n        <div>\n          <p>info@takeaseat.com</p>\n          <p>0800 5353</p>\n          <p>The Netherlands</p>\n          <p>Kalverstraat 34B</p>\n          <p>Amsterdam</p>\n        </div>\n      </div>\n  </ion-item>\n</ion-list>\n</ion-content>\n'/*ion-inline-end:"C:\Users\hicham\Documents\projects\school\smartChair\smartChairApp\src\pages\info\info.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], InfoPage);
    return InfoPage;
}());

//# sourceMappingURL=info.js.map

/***/ }),

/***/ 111:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 111;

/***/ }),

/***/ 152:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/info/info.module": [
		277,
		1
	],
	"../pages/login/login.module": [
		278,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 152;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 153:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__settings_settings__ = __webpack_require__(154);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(155);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__info_info__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__login_login__ = __webpack_require__(41);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var TabsPage = /** @class */ (function () {
    function TabsPage(storage) {
        this.storage = storage;
        this.tab1Root = __WEBPACK_IMPORTED_MODULE_1__settings_settings__["a" /* SettingsPage */];
        this.tab2Root = __WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */];
        this.tab3Root = __WEBPACK_IMPORTED_MODULE_3__info_info__["a" /* InfoPage */];
        this.rootPage = __WEBPACK_IMPORTED_MODULE_5__login_login__["a" /* LoginPage */];
    }
    TabsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"C:\Users\hicham\Documents\projects\school\smartChair\smartChairApp\src\pages\tabs\tabs.html"*/'<ion-tabs selectedIndex=1>\n  <ion-tab [root]="tab1Root" tabTitle="Settings" tabIcon="md-cog"></ion-tab>\n  <ion-tab [root]="tab2Root" tabTitle="Home" tabIcon="md-home"></ion-tab>\n  <ion-tab [root]="tab3Root" tabTitle="info" tabIcon="md-information-circle"></ion-tab>\n</ion-tabs>\n\n'/*ion-inline-end:"C:\Users\hicham\Documents\projects\school\smartChair\smartChairApp\src\pages\tabs\tabs.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */]])
    ], TabsPage);
    return TabsPage;
}());

//# sourceMappingURL=tabs.js.map

/***/ }),

/***/ 154:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SettingsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__login_login__ = __webpack_require__(41);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SettingsPage = /** @class */ (function () {
    function SettingsPage(navCtrl, storage, app) {
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.app = app;
    }
    SettingsPage.prototype.logout = function () {
        this.storage.remove("code");
        var nav = this.app.getRootNav();
        nav.setRoot(__WEBPACK_IMPORTED_MODULE_3__login_login__["a" /* LoginPage */]);
        this.navCtrl.popToRoot();
    };
    SettingsPage.prototype.reset = function () {
        this.storage.remove("settings");
        this.navCtrl.parent.select(1);
    };
    SettingsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-settings',template:/*ion-inline-start:"C:\Users\hicham\Documents\projects\school\smartChair\smartChairApp\src\pages\settings\settings.html"*/'<ion-header>\n  <ion-toolbar>\n    <ion-title>\n      Settings\n    </ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content padding>\n  <ion-list>\n    <ion-title class="wrapper-logo">\n      <ion-img class="img-logo" src="assets/imgs/logo.jpg"></ion-img>\n    </ion-title>\n    <ion-item>\n      <div class="content-title">\n        Settings\n      </div>\n      <div class="content-body" margin-horizontal (click)="logout()">\n        <span class="dot"></span>\n        <h2 class="content-body-title">Disconnect Chair</h2>\n      </div>\n      <div class="content-body" margin-horizontal (click)="reset()">\n        <span class="dot"></span>\n        <h2 class="content-body-title">Reset all saved settings</h2>\n      </div>\n    </ion-item>\n  </ion-list>\n</ion-content>\n'/*ion-inline-end:"C:\Users\hicham\Documents\projects\school\smartChair\smartChairApp\src\pages\settings\settings.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* App */]])
    ], SettingsPage);
    return SettingsPage;
}());

//# sourceMappingURL=settings.js.map

/***/ }),

/***/ 155:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_storage__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, storage) {
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.name = "standard";
        this.settingsHTML = "";
        this.armrestHeight = 0;
        this.seatHeight = 0;
        this.backAngle = 0;
        this.legSupport = 0;
        this.upperMassageIntensity = "low";
        this.lowerMassageIntensity = "low";
        this.leggMassageIntensity = "low";
        this.screen = false;
        this.brightness = 0;
        this.contract = 0;
        this.sharpness = 0;
        this.gamma = 0;
        this.upperTemp = 0;
        this.lowerTemp = 0;
        this.leggTemp = 0;
        this.globalVolume = 0;
        this.trebble = 0;
        this.bassIntensity = 0;
        this.currentSetting = "standard";
        this.settingsArray = [];
        this.formGroup = new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormGroup */]({
            name: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormControl */]()
        });
    }
    HomePage.prototype.ionViewDidEnter = function () {
        var _this = this;
        var settingsStandard = {
            name: this.name,
            armrestHeight: this.armrestHeight,
            seatHeight: this.seatHeight,
            backAngle: this.backAngle,
            legSupport: this.legSupport,
            upperMassageIntensity: this.upperMassageIntensity,
            lowerMassageIntensity: this.lowerMassageIntensity,
            leggMassageIntensity: this.leggMassageIntensity,
            screen: this.screen,
            brightness: this.brightness,
            contract: this.contract,
            sharpness: this.sharpness,
            gamma: this.gamma,
            upperTemp: this.upperTemp,
            lowerTemp: this.lowerTemp,
            leggTemp: this.leggTemp,
            globalVolume: this.globalVolume,
            trebble: this.trebble,
            bassIntensity: this.bassIntensity
        };
        this.storage.get("settings").then(function (settings) {
            if (settings) {
                settings.forEach(function (setting) {
                    _this.settingsArray.push(setting);
                });
            }
            else {
                _this.settingsArray = [];
                _this.storage.set("settings", [settingsStandard]);
                _this.settingsArray.push(settingsStandard);
            }
        });
    };
    HomePage.prototype.switch = function (event) {
        var setting = event.currentTarget.nextElementSibling;
        var dot = event.currentTarget.children[0];
        if (setting.style.display === "block") {
            setting.style.display = "none";
            this.switchClass(dot, "point-close", "point-open");
        }
        else {
            setting.style.display = "block";
            this.switchClass(dot, "point-open", "point-close");
        }
    };
    HomePage.prototype.switchClass = function (element, class1, class2) {
        if (element.classList.contains(class1)) {
            element.classList.remove(class1);
            element.classList.add(class2);
        }
        else {
            element.classList.remove(class2);
            element.classList.add(class1);
        }
    };
    HomePage.prototype.saveSetting = function (formValues) {
        var _this = this;
        var settings = {
            name: formValues.name,
            armrestHeight: this.armrestHeight,
            seatHeight: this.seatHeight,
            backAngle: this.backAngle,
            legSupport: this.legSupport,
            upperMassageIntensity: this.upperMassageIntensity,
            lowerMassageIntensity: this.lowerMassageIntensity,
            leggMassageIntensity: this.leggMassageIntensity,
            screen: this.screen,
            brightness: this.brightness,
            contract: this.contract,
            sharpness: this.sharpness,
            gamma: this.gamma,
            upperTemp: this.upperTemp,
            lowerTemp: this.lowerTemp,
            leggTemp: this.leggTemp,
            globalVolume: this.globalVolume,
            trebble: this.trebble,
            bassIntensity: this.bassIntensity
        };
        this.settingsArray.push(settings);
        this.storage.set("settings", this.settingsArray).then(function (e) {
            _this.updateSettings();
        });
        var modal = document.querySelector(".modal");
        modal.style.visibility = "hidden";
    };
    HomePage.prototype.showModal = function () {
        var modal = document.querySelector(".modal");
        modal.style.visibility = "visible";
    };
    HomePage.prototype.updateSettings = function () {
        var _this = this;
        this.settingsArray = [];
        this.storage.get("settings").then(function (settings) {
            settings.forEach(function (setting) {
                _this.settingsArray.push(setting);
            });
        });
    };
    HomePage.prototype.update = function () {
        var _this = this;
        this.storage.get("settings").then(function (settings) {
            settings.forEach(function (setting) {
                if (setting.name === _this.currentSetting) {
                    _this.name = setting.name,
                        _this.armrestHeight = setting.armrestHeight,
                        _this.seatHeight = setting.seatHeight,
                        _this.backAngle = setting.backAngle,
                        _this.legSupport = setting.legSupport,
                        _this.upperMassageIntensity = setting.upperMassageIntensity,
                        _this.lowerMassageIntensity = setting.lowerMassageIntensity,
                        _this.leggMassageIntensity = setting.leggMassageIntensity,
                        _this.screen = setting.screen,
                        _this.brightness = setting.brightness,
                        _this.contract = setting.contract,
                        _this.sharpness = setting.sharpness,
                        _this.gamma = setting.gamma,
                        _this.upperTemp = setting.upperTemp,
                        _this.lowerTemp = setting.lowerTemp,
                        _this.leggTemp = setting.leggTemp,
                        _this.globalVolume = setting.globalVolume,
                        _this.trebble = setting.trebble,
                        _this.bassIntensity = setting.bassIntensity;
                }
            });
        });
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"C:\Users\hicham\Documents\projects\school\smartChair\smartChairApp\src\pages\home\home.html"*/'<ion-header>\n  <ion-toolbar>\n    <ion-title>\n      Functions\n    </ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content padding>\n  <ion-list>\n    <ion-title class="wrapper-logo">\n      <ion-img class="img-logo" src="assets/imgs/logo.jpg"></ion-img>\n    </ion-title>\n    <ion-item>\n      <div class="content-title">\n        Functions\n      </div>\n      <div class="content-body" margin-horizontal (click)="switch($event)">\n        <span class="dot point-close"></span>\n        <h2 class="content-body-title">Ergonomic Settings</h2>\n      </div>\n      <div class="content-settings" padding margin-horizontal text-wrap>\n        <div class="setting">\n          <div text-center class="setting-title">\n            <h3>Armrest height</h3>\n            <span>{{armrestHeight}}cm</span>\n          </div>\n          <ion-item>\n            <ion-range min="0" max="30" [(ngModel)]="armrestHeight" pin="true"></ion-range>\n          </ion-item>\n        </div>\n        <div class="setting">\n          <div text-center class="setting-title">\n            <h3>Seat height</h3>\n            <span>{{seatHeight}}cm</span>\n          </div>\n          <ion-item>\n            <ion-range min="0" max="70" [(ngModel)]="seatHeight" pin="true"></ion-range>\n          </ion-item>\n        </div>\n        <div class="setting">\n          <div text-center class="setting-title">\n            <h3>Back Angle Adjustment</h3>\n            <span>{{backAngle}}\n              <span>°</span>\n            </span>\n          </div>\n          <ion-item>\n            <ion-range min="0" max="180" [(ngModel)]="backAngle" pin="true"></ion-range>\n          </ion-item>\n        </div>\n        <div class="setting">\n          <div text-center class="setting-title">\n            <h3>Leg support</h3>\n            <span>{{legSupport}}\n              <span>°</span>\n            </span>\n          </div>\n          <ion-item>\n            <ion-range min="90" max="180" [(ngModel)]="legSupport" pin="true"></ion-range>\n          </ion-item>\n        </div>\n      </div>\n\n      <!-- --------------------------------------------------------------------------- -->\n      <div class="content-body" margin-horizontal text-wrap (click)="switch($event)">\n        <span class="dot point-close"></span>\n        <h2 class="content-body-title">Massage Functions</h2>\n      </div>\n      <div class="content-settings" padding margin-horizontal text-wrap>\n        <div class="setting setting-m">\n          <div class="setting-title">\n            <h3>Upperback Massage Intensity</h3>\n          </div>\n          <div class="radio-group">\n            <label class="radio-sub" text-center>Low\n              <input class="radio-input" type="radio" name="upperMassageIntensity" value="low" [(ngModel)]="upperMassageIntensity" [checked]="upperMassageIntensity === low">\n              <span class="radio-dot"></span>\n            </label>\n            <label class="radio-sub" text-center>Medium\n              <input class="radio-input" type="radio" name="upperMassageIntensity" value="medium" [(ngModel)]="upperMassageIntensity" [checked]="upperMassageIntensity === medium">\n              <span class="radio-dot"></span>\n            </label>\n            <label class="radio-sub" text-center>High\n              <input class="radio-input" type="radio" name="upperMassageIntensity" value="high" [(ngModel)]="upperMassageIntensity" [checked]="upperMassageIntensity === high">\n              <span class="radio-dot"></span>\n            </label>\n          </div>\n        </div>\n        <div class="setting setting-m">\n          <div class="setting-title">\n            <h3>Lowerback Massage Intensity</h3>\n          </div>\n          <div class="radio-group">\n            <label class="radio-sub" text-center>Low\n              <input class="radio-input" type="radio" name="lowerMassageIntensity" value="low" [(ngModel)]="lowerMassageIntensity" [checked]="true">\n              <span class="radio-dot"></span>\n            </label>\n            <label class="radio-sub" text-center>Medium\n              <input class="radio-input" type="radio" name="lowerMassageIntensity" value="medium" [(ngModel)]="lowerMassageIntensity">\n              <span class="radio-dot"></span>\n            </label>\n            <label class="radio-sub" text-center>High\n              <input class="radio-input" type="radio" name="lowerMassageIntensity" value="high" [(ngModel)]="lowerMassageIntensity">\n              <span class="radio-dot"></span>\n            </label>\n          </div>\n        </div>\n        <div class="setting setting-m">\n          <div class="setting-title">\n            <h3>Legg Massage Intensity</h3>\n          </div>\n          <div class="radio-group">\n            <label class="radio-sub" text-center>Low\n              <input class="radio-input" type="radio" name="leggMassageIntensity" value="low" [(ngModel)]="leggMassageIntensity" [checked]="true">\n              <span class="radio-dot"></span>\n            </label>\n            <label class="radio-sub" text-center>Medium\n              <input class="radio-input" type="radio" name="leggMassageIntensity" value="medium" [(ngModel)]="leggMassageIntensity">\n              <span class="radio-dot"></span>\n            </label>\n            <label class="radio-sub" text-center>High\n              <input class="radio-input" type="radio" name="leggMassageIntensity" value="high" [(ngModel)]="leggMassageIntensity">\n              <span class="radio-dot"></span>\n            </label>\n          </div>\n        </div>\n      </div>\n\n      <!-- --------------------------------------------------------------------------- -->\n      <div class="content-body" margin-horizontal text-wrap (click)="switch($event)">\n        <span class="dot point-close"></span>\n        <h2 class="content-body-title">Screen settings</h2>\n      </div>\n      <div class="content-settings" padding margin-horizontal text-wrap>\n        <div class="setting">\n          <div class="radio-group">\n            <label class="radio-sub" text-center>Hide screen\n              <input class="radio-input" type="radio" name="screen" [value]="false" [(ngModel)]="screen" [checked]="true">\n              <span class="radio-dot"></span>\n            </label>\n            <label class="radio-sub">Show screen\n              <input class="radio-input" type="radio" name="screen" [value]="true" [(ngModel)]="screen">\n              <span class="radio-dot"></span>\n            </label>\n          </div>\n          <div class="setting">\n            <div text-center class="setting-title">\n              <h3>Brightness</h3>\n            </div>\n            <ion-item>\n              <ion-range min="0" max="100" [(ngModel)]="brightness"></ion-range>\n            </ion-item>\n          </div>\n          <div class="setting">\n            <div text-center class="setting-title">\n              <h3>Contract</h3>\n            </div>\n            <ion-item>\n              <ion-range min="0" max="100" [(ngModel)]="contract"></ion-range>\n            </ion-item>\n          </div>\n          <div class="setting">\n            <div text-center class="setting-title">\n              <h3>Sharpness</h3>\n            </div>\n            <ion-item>\n              <ion-range min="0" max="100" [(ngModel)]="sharpness"></ion-range>\n            </ion-item>\n          </div>\n          <div class="setting">\n            <div text-center class="setting-title">\n              <h3>Gamma</h3>\n            </div>\n            <ion-item>\n              <ion-range min="0" max="100" [(ngModel)]="gamma"></ion-range>\n            </ion-item>\n          </div>\n        </div>\n      </div>\n\n      <div class="content-body" margin-horizontal (click)="switch($event)">\n        <span class="dot point-close"></span>\n        <h2 class="content-body-title">Temperature</h2>\n      </div>\n      <div class="content-settings" padding margin-horizontal text-wrap>\n        <div class="setting">\n          <div text-center class="setting-title">\n            <h3>Upperback Temperature</h3>\n          </div>\n          <ion-item>\n            <ion-range min="0" max="38" [(ngModel)]="upperTemp"></ion-range>\n          </ion-item>\n          <div class="icons">\n            <ion-icon name="snow"></ion-icon>\n            <ion-icon name="snow"></ion-icon>\n            <ion-icon name="snow"></ion-icon>\n            <ion-icon name="flame"></ion-icon>\n            <ion-icon name="flame"></ion-icon>\n            <ion-icon name="flame"></ion-icon>\n          </div>\n        </div>\n        <div class="setting">\n          <div text-center class="setting-title">\n            <h3>Lowerback Temperature</h3>\n          </div>\n          <ion-item>\n            <ion-range min="0" max="38" [(ngModel)]="lowerTemp"></ion-range>\n          </ion-item>\n          <div class="icons">\n            <ion-icon name="snow"></ion-icon>\n            <ion-icon name="snow"></ion-icon>\n            <ion-icon name="snow"></ion-icon>\n            <ion-icon name="flame"></ion-icon>\n            <ion-icon name="flame"></ion-icon>\n            <ion-icon name="flame"></ion-icon>\n          </div>\n        </div>\n        <div class="setting">\n          <div text-center class="setting-title">\n            <h3>Legg Temperature</h3>\n          </div>\n          <ion-item>\n            <ion-range min="0" max="38" [(ngModel)]="leggTemp"></ion-range>\n          </ion-item>\n          <div class="icons">\n            <ion-icon name="snow"></ion-icon>\n            <ion-icon name="snow"></ion-icon>\n            <ion-icon name="snow"></ion-icon>\n            <ion-icon name="flame"></ion-icon>\n            <ion-icon name="flame"></ion-icon>\n            <ion-icon name="flame"></ion-icon>\n          </div>\n        </div>\n      </div>\n\n      <div class="content-body" margin-horizontal (click)="switch($event)">\n        <span class="dot point-close"></span>\n        <h2 class="content-body-title">Audio options</h2>\n      </div>\n      <div class="content-settings" padding margin-horizontal text-wrap>\n        <div class="setting">\n          <div text-center class="setting-title">\n            <h3>Global Volume</h3>\n          </div>\n          <div class="range-slider">\n            <ion-item>\n              <ion-range min="1" max="10" [(ngModel)]="globalVolume"></ion-range>\n            </ion-item>\n          </div>\n        </div>\n        <div class="setting">\n          <div text-center class="setting-title">\n            <h3>Trebble</h3>\n          </div>\n          <div class="range-slider">\n            <ion-item>\n              <ion-range min="1" max="10" [(ngModel)]="trebble"></ion-range>\n            </ion-item>\n          </div>\n        </div>\n        <div class="setting">\n          <div text-center class="setting-title">\n            <h3>Bass Intensity</h3>\n          </div>\n          <div class="range-slider">\n            <ion-item>\n              <ion-range min="1" max="10" [(ngModel)]="bassIntensity"></ion-range>\n            </ion-item>\n          </div>\n        </div>\n      </div>\n\n      <div class="content-body" margin-horizontal (click)="switch($event)">\n        <span class="dot  point-close"></span>\n        <h2 class="content-body-title">Profiles</h2>\n      </div>\n      <div class="content-settings" padding margin-horizontal text-wrap>\n        <div class="radio-group">\n          <label *ngFor="let setting of settingsArray" class="radio-sub">{{setting.name}}\n            <input class="radio-input" type="radio" name="setting" value="{{setting.name}}" [checked]="setting.name === name" (ngModelChange)="update()"\n              [(ngModel)]="currentSetting">\n            <span class="radio-dot"></span>\n          </label>\n          <label class="radio-sub save-setting">Save current setting\n            <!-- <input class="radio-input" type="radio" [checked]="false"> -->\n            <span class="radio-dot" (click)="showModal()"><ion-icon name="md-add"></ion-icon></span>\n          </label>\n        </div>\n      </div>\n    </ion-item>\n  </ion-list>\n</ion-content>\n<div class="modal">\n  <form [formGroup]="formGroup" (ngSubmit)="saveSetting(formGroup.value)">\n    <ion-item>\n      <ion-label>Name</ion-label>\n      <ion-input type="text" name="name" formControlName="name" required></ion-input>\n    </ion-item>\n    <button ion-button type="submit" [disabled]="!formGroup.valid">Save Note</button>\n  </form>\n</div>\n'/*ion-inline-end:"C:\Users\hicham\Documents\projects\school\smartChair\smartChairApp\src\pages\home\home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1__ionic_storage__["b" /* Storage */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 199:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(222);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 222:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__(268);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_home_home__ = __webpack_require__(155);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_tabs_tabs__ = __webpack_require__(153);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_settings_settings__ = __webpack_require__(154);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_info_info__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_login_login__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_status_bar__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_native_splash_screen__ = __webpack_require__(198);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};












var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_7__pages_settings_settings__["a" /* SettingsPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_info_info__["a" /* InfoPage */],
                __WEBPACK_IMPORTED_MODULE_5__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_6__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_login_login__["a" /* LoginPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/info/info.module#InfoPageModule', name: 'InfoPage', segment: 'info', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["a" /* IonicStorageModule */].forRoot()
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_7__pages_settings_settings__["a" /* SettingsPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_info_info__["a" /* InfoPage */],
                __WEBPACK_IMPORTED_MODULE_5__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_6__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_login_login__["a" /* LoginPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_10__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_11__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicErrorHandler */] }
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 268:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_login_login__ = __webpack_require__(41);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_login_login__["a" /* LoginPage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"C:\Users\hicham\Documents\projects\school\smartChair\smartChairApp\src\app\app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"C:\Users\hicham\Documents\projects\school\smartChair\smartChairApp\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 41:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__tabs_tabs__ = __webpack_require__(153);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(40);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl, storage) {
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.showKeyboard = false;
        this.codeArray = [];
        this.code = [];
    }
    LoginPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.storage.get("code").then(function (element) {
            if (element !== null) {
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__tabs_tabs__["a" /* TabsPage */]);
            }
        });
    };
    LoginPage.prototype.enterCode = function (code) {
        var img = document.querySelector(".img-logo");
        for (var i = 1; i <= code.children.length; i++) {
            this.code.push(code.children.item(i));
        }
        if (img !== null) {
            img.remove();
            this.showKeyboard = true;
        }
        else {
            this.showKeyboard = true;
        }
    };
    LoginPage.prototype.enterNumbers = function (element) {
        if (parseInt(element.target.textContent) || parseInt(element.target.textContent) === 0) {
            var num = parseInt(element.target.textContent);
            if (this.codeArray.length <= 4) {
                this.addNum(num);
                if (this.codeArray.length === 5)
                    this.login();
            }
        }
        else {
            this.deleteNum();
        }
    };
    LoginPage.prototype.addNum = function (num) {
        this.codeArray.push(num);
        for (var i = 0; i < this.codeArray.length; i++) {
            this.code[i].textContent = this.codeArray[i].toString();
        }
    };
    LoginPage.prototype.deleteNum = function () {
        this.codeArray.pop();
        for (var i = 0; i < 5; i++) {
            this.code[i].textContent = this.codeArray[i] === undefined ? "" : this.codeArray[i].toString();
        }
    };
    LoginPage.prototype.login = function () {
        this.storage.set("code", this.codeArray);
        this.codeArray = [];
        for (var i = 0; i < 5; i++) {
            this.code[i].textContent = this.codeArray[i] === undefined ? "" : this.codeArray[i].toString();
        }
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__tabs_tabs__["a" /* TabsPage */]);
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"C:\Users\hicham\Documents\projects\school\smartChair\smartChairApp\src\pages\login\login.html"*/'<ion-header>\n\n  <ion-navbar>\n    <ion-title>Take a\' Seat</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n  <ion-grid>\n    <ion-row class="wrapper-logo">\n      <ion-col>\n        <ion-img class="img-logo" src="assets/imgs/logo.jpg" alt="Logo"></ion-img>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col>\n        <div class="header-title">Lets connect your chair!</div>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col>\n        <p class="header-text">Enter the number which you can find on the bottom of the box</p>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col>\n        <div #code (click)="enterCode(code)" class="wrapper-code">\n          <div class="line"></div>\n          <div class="numbers"></div>\n          <div class="numbers"></div>\n          <div class="numbers"></div>\n          <div class="numbers"></div>\n          <div class="numbers"></div>\n        </div>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n  <!-- Keyboard -->\n  <ion-grid (click)="enterNumbers($event)" class="kb" *ngIf="showKeyboard">\n    <ion-row>\n      <ion-col>\n        <div class="kb-num">\n          <span>1</span>\n        </div>\n      </ion-col>\n      <ion-col>\n        <div class="kb-num">\n          <span>2</span>\n        </div>\n      </ion-col>\n      <ion-col>\n        <div class="kb-num">\n          <span>3</span>\n        </div>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col>\n        <div class="kb-num">\n          <span>4</span>\n        </div>\n      </ion-col>\n      <ion-col>\n        <div class="kb-num">\n          <span>5</span>\n        </div>\n      </ion-col>\n      <ion-col>\n        <div class="kb-num">\n          <span>6</span>\n        </div>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col>\n        <div class="kb-num">\n          <span>7</span>\n        </div>\n      </ion-col>\n      <ion-col>\n        <div class="kb-num">\n          <span>8</span>\n        </div>\n      </ion-col>\n      <ion-col>\n        <div class="kb-num">\n          <span>9</span>\n        </div>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col>\n        <div class="kb-num">\n          <span></span>\n        </div>\n      </ion-col>\n      <ion-col>\n        <div class="kb-num">\n          <span>0</span>\n        </div>\n      </ion-col>\n      <ion-col>\n        <div class="kb-num">\n          <span>\n            <ion-icon name="arrow-round-back"></ion-icon>\n          </span>\n        </div>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n</ion-content>\n'/*ion-inline-end:"C:\Users\hicham\Documents\projects\school\smartChair\smartChairApp\src\pages\login\login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ })

},[199]);
//# sourceMappingURL=main.js.map