"use strict";
document.addEventListener("DOMContentLoaded", function () {
    var playButton = document.querySelector(".play-video");
    var stopButton = document.querySelector(".stop-video");
    var video = document.querySelector("video");
    var overlay = document.querySelector(".header-overlay");
    if (playButton && video && stopButton) {
        playButton.addEventListener("click", function () {
            video.play();
            playButton.style.visibility = "hidden";
            stopButton.style.visibility = "visible";
            overlay.style.visibility = "hidden";
        });
        stopButton.addEventListener("click", function () {
            video.pause();
            video.currentTime = 0;
            playButton.style.visibility = "visible";
            stopButton.style.visibility = "hidden";
            overlay.style.visibility = "visible";
        });
    }
    else {
        console.error("something is null!");
    }
});
