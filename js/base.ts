document.addEventListener("DOMContentLoaded", () => {
    const playButton = document.querySelector(".play-video");
    const stopButton = document.querySelector(".stop-video");
    const video = document.querySelector("video");
    const overlay = document.querySelector(".header-overlay");
    if (playButton && video && stopButton) {
        playButton.addEventListener("click", () => {
            video.play();
            (playButton as HTMLElement).style.visibility = "hidden";
            (stopButton as HTMLElement).style.visibility = "visible";
            (overlay as HTMLElement).style.visibility = "hidden";
        });

        stopButton.addEventListener("click", () => {
            video.pause();
            video.currentTime = 0;
            (playButton as HTMLElement).style.visibility = "visible";
            (stopButton as HTMLElement).style.visibility = "hidden";
            (overlay as HTMLElement).style.visibility = "visible";
        });
    } else {
        console.error("something is null!");
    }
});
